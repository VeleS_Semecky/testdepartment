package com.example.yurii.mydepartment.room.dao.core;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.yurii.mydepartment.room.dao.DepartmentDAO;
import com.example.yurii.mydepartment.room.dao.FieldDAO;
import com.example.yurii.mydepartment.room.dao.PolygonDAO;
import com.example.yurii.mydepartment.room.model.DepartmentTable;
import com.example.yurii.mydepartment.room.model.FieldTable;
import com.example.yurii.mydepartment.room.model.PolygonTable;

@Database(entities = {DepartmentTable.class, FieldTable.class, PolygonTable.class}, version = 1, exportSchema = false)
public abstract class RoomDatabase extends android.arch.persistence.room.RoomDatabase {

    public abstract DepartmentDAO departmentDAO();
    public abstract FieldDAO fieldDAO();
    public abstract PolygonDAO polygonDAO();

    private static RoomDatabase INSTANCE;

    public static RoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabase.class, "room_database")
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            // Migration is not part of this codelab.
//                            .fallbackToDestructiveMigration()
                            .build();

                }
            }
        }
        return INSTANCE;
    }
}
