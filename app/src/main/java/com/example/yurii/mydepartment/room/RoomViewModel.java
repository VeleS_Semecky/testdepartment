package com.example.yurii.mydepartment.room;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.yurii.mydepartment.room.model.DepartmentTable;
import com.example.yurii.mydepartment.room.model.FieldTable;
import com.example.yurii.mydepartment.room.model.FieldsAndPolygons;
import com.example.yurii.mydepartment.room.model.PolygonTable;

import java.util.List;

public class RoomViewModel extends AndroidViewModel {
    private Repository mRepository;

    private LiveData<List<DepartmentTable>> mAllDepartment;
    private LiveData<List<FieldTable>> mAllFields;
    private LiveData<List<PolygonTable>> mAllPolygons;

    public RoomViewModel(Application application) {
        super(application);
        mRepository = new Repository(application);
        mAllDepartment = mRepository.getmAllDepartment();
    }

    public LiveData<List<DepartmentTable>> getAllDepartments() { return mAllDepartment; }

    public LiveData<List<FieldTable>> getAllFieldStrings(String name) { return mRepository.getField(name); }
    public LiveData<List<FieldTable>> getFieldNameDepartment(String name) { return mRepository.getFieldNameDepartment(name); }
    public LiveData<List<FieldTable>> getAllFields() { return mRepository.getmAllFields(); }
    public LiveData<List<FieldsAndPolygons>> getFieldsAndPolygons(String name) { return mRepository.getFieldsAndPolygons(name); }


    public LiveData<List<PolygonTable>> getAllPoligons(String name) { return mRepository.getAllPolygon(name) ; }



    public void insertDepartmentTable(List<DepartmentTable> word) { mRepository.insertRxListDepartment(word); }
    public void insertFieldTable(List<FieldTable> word) { mRepository.insertRxListField(word); }
    public void insertPolygonTable(List<PolygonTable> word) { mRepository.insertRxListPolygon(word); }

    @Override
    protected void onCleared() {
        mRepository.unsubscribeRxJava();
        super.onCleared();
    }
}
