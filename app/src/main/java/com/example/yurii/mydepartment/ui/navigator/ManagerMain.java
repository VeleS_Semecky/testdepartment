package com.example.yurii.mydepartment.ui.navigator;


import android.support.annotation.NonNull;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.ui.activity.core.BaseActivity;
import com.example.yurii.mydepartment.ui.fragment.ChoiceFragment_;
import com.example.yurii.mydepartment.ui.fragment.InfoPolygonFragment_;
import com.example.yurii.mydepartment.ui.fragment.ListFieldFragment_;
import com.example.yurii.mydepartment.ui.fragment.MapBoxFragment;
import com.example.yurii.mydepartment.ui.fragment.MapBoxFragment_;
import com.example.yurii.mydepartment.ui.fragment.core.BaseFragment;
import com.example.yurii.mydepartment.ui.navigator.core.BaseManager;
import com.example.yurii.mydepartment.ui.navigator.core.ResourceManager;
import com.example.yurii.mydepartment.ui.navigator.core.ResourceNames;


import org.androidannotations.api.builder.FragmentBuilder;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ManagerMain extends BaseManager {

    ManagerMain(BaseFragment baseFragment, BaseActivity baseActivity) {
        super(baseFragment, baseActivity);
    }

    @Override
    public void moveFragmentTo(int id, Object... o) {
        switch (id){
            case ResourceManager.FragmentId.LIST_FIELD_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerTwo, ListFieldFragment_.builder().build(), ResourceNames.LIST_FIELD_FRAGMENT).commit();
                break;
            case ResourceManager.FragmentId.CHOICE_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerTwo, ChoiceFragment_.builder().build(), ResourceNames.CHOICE_FRAGMENT).commit();
                break;
            case ResourceManager.FragmentId.MAPBOX_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().add(R.id.containerTwo, MapBoxFragment_.builder().build(), ResourceNames.MAPBOX_FRAGMENT).commit();
                break;
            case ResourceManager.FragmentId.INFO_POLYGON_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerTwo, InfoPolygonFragment_.builder().build(), ResourceNames.INFO_POLYGON_FRAGMENT).commit();
                break;
//            case ResourceManager.FragmentId.LIST_PRODUCT_FRAGMENT:
//                replaceFragment(R.id.fragment_container, ListProductsFragment_.builder(),o[0].toString(),ResourceNames.LIST_PRODUCTS_FRAGMENT);
//                break;

        }
    }

    private void replaceFragment(int layoutFragment, FragmentBuilder fragmentBuilder, @NonNull String bundle, String resourceName){
        baseActivity.getFragmentManager().beginTransaction().replace(layoutFragment, (android.app.Fragment) fragmentBuilder.arg("bundle",bundle).build(), resourceName).commit();
    }

    private void replaceFragment(int layoutFragment, FragmentBuilder fragmentBuilder, int bundle, String resourceName){
        baseActivity.getFragmentManager().beginTransaction().replace(layoutFragment, (android.app.Fragment) fragmentBuilder.arg("bundle",bundle).build(), resourceName).commit();
    }

    @Override
    public void removeFragment() {
            baseFragment.getActivity().getFragmentManager().beginTransaction().remove(baseFragment).commit();
    }
}
