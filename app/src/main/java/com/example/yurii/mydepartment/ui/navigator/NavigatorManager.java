package com.example.yurii.mydepartment.ui.navigator;



import com.example.yurii.mydepartment.ui.fragment.core.BaseFragment;
import com.example.yurii.mydepartment.ui.navigator.core.Manager;
import com.example.yurii.mydepartment.ui.navigator.core.NavigatorBaseManager;

import org.androidannotations.annotations.EBean;

/**
 * Created by Юрий on 24.03.2018.
 */
@EBean
public class NavigatorManager extends NavigatorBaseManager {

    @Override
    public Manager getMainManager(BaseFragment baseFragment) {

        return new ManagerMain(baseFragment,baseActivity);

    }

}
