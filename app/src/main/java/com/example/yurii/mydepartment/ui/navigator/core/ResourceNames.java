package com.example.yurii.mydepartment.ui.navigator.core;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ResourceNames {

    public static final String LIST_FIELD_FRAGMENT = "ListFieldFragment";
    public static final String CHOICE_FRAGMENT = "ChoiceFragment";
    public static final String MAPBOX_FRAGMENT = "MapBoxFragment";
    public static final String INFO_POLYGON_FRAGMENT = "InfoPolygonFragment";



}
