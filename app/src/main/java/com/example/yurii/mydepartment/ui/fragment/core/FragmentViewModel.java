package com.example.yurii.mydepartment.ui.fragment.core;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

public class FragmentViewModel extends AndroidViewModel {
    private MutableLiveData<String> mDepartment = new MutableLiveData<>();
    private MutableLiveData<String> mField = new MutableLiveData<>();

    public MutableLiveData<String> getmDepartment() {
        return mDepartment;
    }

    public MutableLiveData<String> getmField() {
        return mField;
    }

    public FragmentViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}
