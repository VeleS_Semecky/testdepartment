package com.example.yurii.mydepartment.ui.navigator.core;

import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.ui.activity.core.BaseActivity;
import com.example.yurii.mydepartment.ui.fragment.core.BaseFragment;


/**
 * Created by Юрий on 24.03.2018.
 */

public abstract class BaseManager implements Manager {

    protected BaseFragment baseFragment;
    protected BaseActivity baseActivity;
    public BaseManager(BaseFragment baseFragment, BaseActivity baseActivity) {
        this.baseFragment = baseFragment;
        this.baseActivity = baseActivity;
    }


    @Override
    public void initToolbar(int id, Object... text) {
        View inflate = null;
        switch (id) {
            case ResourceManager.ToolbarId.SIMPLE:
                inflate = baseActivity.getLayoutInflater().inflate(R.layout.toolbar_simple, null,false);

                if(text.length > 0){
                    ((TextView)inflate.findViewById(R.id.text)).setText((CharSequence) text[0]);

                    inflate.findViewById(R.id.text).setOnClickListener(v -> baseFragment.clickToolbar(1));
                }
                break;

//            case ResourceManager.ToolbarId.SEARCH:
//                inflate = baseActivity.getLayoutInflater().inflate(R.layout.toolbar_simple, null, false);
//                if (text.length > 0) {
//                    ((android.support.v7.widget.SearchView) inflate.findViewById(R.id.searching)).setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
//                        @Override
//                        public boolean onQueryTextSubmit(String query) {
//                            Toast.makeText(baseActivity, "Our word : " + query, Toast.LENGTH_SHORT).show();
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onQueryTextChange(String newText) {
//                            Toast.makeText(baseActivity, "Our words : " + newText, Toast.LENGTH_SHORT).show();
//                            return false;
//                        }
//                    });
//            case ResourceManager.ToolbarId.PRODUCT:
//                inflate = baseActivity.getLayoutInflater().inflate(R.layout.toolbar_product, null,false);
//                if(text.length > 0){
//                    ((TextView)inflate.findViewById(R.id.txtNameShop)).setText(text[0].toString());
//                    inflate.findViewById(R.id.txtNameShop).setOnClickListener(v -> baseFragment.clickToolbar(R.id.txtNameShop));
//                }
//                inflate.findViewById(R.id.btnBack).setOnClickListener(v -> baseFragment.clickToolbar(R.id.btnBack));
//                inflate.findViewById(R.id.btnPay).setOnClickListener(v -> baseFragment.clickToolbar(R.id.btnPay));
//                break;
//
//                }
//                break;

            //add view
        }

                baseFragment.getToolbarLayout().removeAllViews();
                baseFragment.getToolbarLayout().addView(inflate);

    }



}
