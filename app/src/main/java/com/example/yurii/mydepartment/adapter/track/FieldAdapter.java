package com.example.yurii.mydepartment.adapter.track;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.example.yurii.mydepartment.adapter.bean.finder.AddFieldInAdapter;
import com.example.yurii.mydepartment.adapter.core.RecyclerViewAdapterBase;
import com.example.yurii.mydepartment.adapter.core.ViewWrapper;
import com.example.yurii.mydepartment.room.RoomViewModel;
import com.example.yurii.mydepartment.room.model.FieldTable;
import com.example.yurii.mydepartment.ui.activity.core.BaseActivity;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

@EBean
public class FieldAdapter extends RecyclerViewAdapterBase<FieldTable, FieldItemView> {

    @RootContext
    BaseActivity baseActivity;

    @Bean
    AddFieldInAdapter addFieldInAdapter;


    @AfterViews
    void initAdapter(){
        RoomViewModel mRoomViewModel;
        // Get a new or existing ViewModel from the ViewModelProvider.
        mRoomViewModel = ViewModelProviders.of(baseActivity).get(RoomViewModel.class);
        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mRoomViewModel.insertDepartmentTable(addFieldInAdapter.setDepartmentTables());
        mRoomViewModel.insertFieldTable(addFieldInAdapter.setFieldTables());
        mRoomViewModel.insertPolygonTable(addFieldInAdapter.setPolygonTables());

        mRoomViewModel.getAllFields().observe(baseActivity, new Observer<List<FieldTable>>() {
            @Override
            public void onChanged(@Nullable List<FieldTable> fieldTables) {
                setItem(fieldTables);
            }
        });
//        mRoomViewModel.getAllTracks().observe(baseActivity, new Observer<List<DepartmentTable>>(){
//
//            @Override
//            public void onChanged(@Nullable List<DepartmentTable> departmentTables) {
//                setItem(departmentTables);
//            }
//        });
        //TODO:add
//        mRoomViewModel.insert(trackBeanFinder.getTrack());
//        setItem(trackBeanFinder.getTrack());

//        mRoomViewModel.delete(getValue().get(1));

//        trackExoFinder.startService();
//        trackBeanFinder.prepareExoPlayerFromFileUri("");
    }

    @Override
    protected FieldItemView onCreateItemHolder(ViewGroup parent, int viewType) {
        return FieldItemView_.build(baseActivity);
    }



//    @Override
//    public void onBindViewHolder(@NonNull ViewWrapper<FieldItemView> holder, int position) {
//
//        FieldItemView view = holder.getView();
//        Track track = getItem().get(position);
//        view.bind(track);
////        view.setPlayerInItem(trackExoFinder);
//        view.setTag(track);
////                view.setOnClickListener(v -> trackExoFinder.playMysic());
//
////        view.setOnClickListener(v -> { trackBeanFinder.prepareExoPlayerFromFileUri(trackStart.getName());});
////        view.setOnClickListener(v -> trackExoFinder.startMyService(trackStart.getName()));


    @Override
    public void onBindViewHolder(@NonNull ViewWrapper<FieldItemView> holder, int position) {
        FieldItemView view = holder.getView();
        FieldTable fieldTable = getItem().get(position);
        view.bind(fieldTable);
        view.setTag(position);
//        ProgressDialogInMyApp progressDialogInMyApp = new ProgressDialogInMyApp(baseActivity,fieldTable.getNameField());
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                progressDialogInMyApp.showDialog();
//            }
//        });
    }
}
