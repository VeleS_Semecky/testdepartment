package com.example.yurii.mydepartment.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.yurii.mydepartment.room.model.PolygonTable;

import java.util.List;
@Dao
public interface PolygonDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllField(List<PolygonTable> people);

    @Delete
    void delete(PolygonTable person);


    @Query("SELECT * FROM polygon_table WHERE nameMyField IS :nameMyField")
    LiveData<List<PolygonTable>> getAllPolygon(String nameMyField);

//    @Query("SELECT  FROM polygon_table WHERE nameMyField IS :nameDepartment")
//    LiveData<List<PolygonTable>> getAllPolygonWithDepartment(String nameDepartment);
}
