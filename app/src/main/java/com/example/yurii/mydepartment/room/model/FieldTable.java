package com.example.yurii.mydepartment.room.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(
        entity = DepartmentTable.class,
        parentColumns = "nameDepartment",
        childColumns = "nameMyDepartment"),tableName = "field_table",indices = {@Index(value = {"nameField"}, unique = true),@Index("nameMyDepartment")})
public class FieldTable {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private int id = 0;

    @ColumnInfo(name = "nameField")
    private String nameField;

    @ColumnInfo(name = "nameMyDepartment")
    private String nameMyDepartment;

    public FieldTable(String nameField, String nameMyDepartment) {
        this.nameField = nameField;
        this.nameMyDepartment = nameMyDepartment;
    }

    public String getNameField() {
        return nameField;
    }

    public void setNameField(String nameField) {
        this.nameField = nameField;
    }

    public String getNameMyDepartment() {
        return nameMyDepartment;
    }

    public void setNameMyDepartment(String nameMyDepartment) {
        this.nameMyDepartment = nameMyDepartment;
    }
}


