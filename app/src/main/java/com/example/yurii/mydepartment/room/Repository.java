package com.example.yurii.mydepartment.room;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.util.Log;

import com.example.yurii.mydepartment.room.dao.core.RoomDatabase;
import com.example.yurii.mydepartment.room.dao.DepartmentDAO;
import com.example.yurii.mydepartment.room.dao.FieldDAO;
import com.example.yurii.mydepartment.room.dao.PolygonDAO;
import com.example.yurii.mydepartment.room.model.DepartmentTable;
import com.example.yurii.mydepartment.room.model.FieldTable;
import com.example.yurii.mydepartment.room.model.FieldsAndPolygons;
import com.example.yurii.mydepartment.room.model.PolygonTable;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Repository {
    private DepartmentDAO mDepartmentDao;
    private FieldDAO mFieldDAO;
    private PolygonDAO mPolygonDAO;




    private LiveData<List<DepartmentTable>> mAllDepartment;
    private LiveData<List<FieldTable>> mAllFields;
    private LiveData<List<PolygonTable>> mAllPolygons;
    private final CompositeDisposable mDisposable = new CompositeDisposable();


    public Repository(Context application) {
        RoomDatabase db = RoomDatabase.getDatabase(application);
        mDepartmentDao = db.departmentDAO();
        mFieldDAO = db.fieldDAO();
        mPolygonDAO = db.polygonDAO();
        mAllDepartment = mDepartmentDao.getAllDepartment();
        mAllFields = mFieldDAO.getAllFields();
    }

    public LiveData<List<FieldTable>> getmAllFields() {
        return mAllFields;
    }

    public LiveData<List<DepartmentTable>> getmAllDepartment() {
        return mAllDepartment;
    }

    public LiveData<List<FieldTable>> getField(String nameField) {
        return mFieldDAO.getField(nameField);
    }
    public LiveData<List<FieldTable>> getFieldNameDepartment(String nameDepartment) {
        return mFieldDAO.getFieldNameDepartment(nameDepartment);
    }

    public LiveData<List<FieldsAndPolygons>> getFieldsAndPolygons(String nameDepartment) {
        return mFieldDAO.getFieldsAndPolygons(nameDepartment);
    }

    public LiveData<List<PolygonTable>> getAllPolygon(String nameMyField) {
        return mPolygonDAO.getAllPolygon(nameMyField);
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.


    private Completable updateListDepartment(List<DepartmentTable> track) {
        return Completable.fromAction(() -> {
//            mAllTracks = track == null
//                    ? new User(userName, userAge, userSex)
//                    : new User(mUser.getId(), userName, userAge, userSex);
            mDepartmentDao.insertAllDepartmen(track);
        });
    }
    private Completable updateListField(List<FieldTable> track) {
        return Completable.fromAction(() -> {
//            mAllTracks = track == null
//                    ? new User(userName, userAge, userSex)
//                    : new User(mUser.getId(), userName, userAge, userSex);
            mFieldDAO.insertAllField(track);
        });
    }
    private Completable updateListPolygon(List<PolygonTable> track) {
        return Completable.fromAction(() -> {
//            mAllTracks = track == null
//                    ? new User(userName, userAge, userSex)
//                    : new User(mUser.getId(), userName, userAge, userSex);
            mPolygonDAO.insertAllField(track);
        });
    }

    public void insertRxListDepartment(List<DepartmentTable> track) {
        mDisposable.add(updateListDepartment(track)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.e("Log", "Update"),
                        throwable -> Log.e("Log", "Unable to update username", throwable)));

    }
    public void insertRxListField(List<FieldTable> track) {
        mDisposable.add(updateListField(track)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.e("Log", "Update"),
                        throwable -> Log.e("Log", "Unable to update username", throwable)));

    }
    public void insertRxListPolygon(List<PolygonTable> track) {
        mDisposable.add(updateListPolygon(track)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.e("Log", "Update"),
                        throwable -> Log.e("Log", "Unable to update username", throwable)));

    }


    public void unsubscribeRxJava(){
        if(!mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
    }
}
