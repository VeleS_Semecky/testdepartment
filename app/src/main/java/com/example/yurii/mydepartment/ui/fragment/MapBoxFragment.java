package com.example.yurii.mydepartment.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.room.RoomViewModel;
import com.example.yurii.mydepartment.room.model.FieldTable;
import com.example.yurii.mydepartment.room.model.FieldsAndPolygons;
import com.example.yurii.mydepartment.room.model.PolygonTable;
import com.example.yurii.mydepartment.ui.fragment.core.BaseFragment;
import com.example.yurii.mydepartment.ui.fragment.core.FragmentViewModel;
import com.example.yurii.mydepartment.ui.navigator.core.ResourceManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Polygon;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EFragment(R.layout.polygon)
public class MapBoxFragment extends BaseFragment implements MapboxMap.OnPolygonClickListener {
    @ViewById(R.id.mapView)
    protected MapView mapView;
    protected FragmentViewModel mFragmentViewModel;
    protected RoomViewModel mRoomViewModel;

    @AfterViews
    public void addMapBox(){
        mFragmentViewModel = ViewModelProviders.of(getBaseActivity()).get(FragmentViewModel.class);
        mRoomViewModel = ViewModelProviders.of(getBaseActivity()).get(RoomViewModel.class);
        Mapbox.getInstance(getBaseActivity(), getString(R.string.access_token));
        mapView.onCreate(null);
        mapView.getMapAsync(this::drawPolygon);
    }

    private void drawPolygon(MapboxMap mapboxMap) {

        mapboxMap.setOnPolygonClickListener(this);
        List<LatLng> polygon = new ArrayList<>();
        if(mFragmentViewModel.getmField().getValue().equals("")) {
            LatLng patagonia = new LatLng(45.622585, -122.785699);
            mapboxMap.setCameraPosition(new CameraPosition.Builder()
                    .target(patagonia)
                    .zoom(11)
                    .build());
            Log.e("MapboxMapgetAll1","OK");
            mRoomViewModel.getFieldNameDepartment(mFragmentViewModel.getmDepartment().getValue()).observe(getBaseActivity(), fieldTables -> {
                assert fieldTables != null;
                for (FieldTable fieldTable : fieldTables) {
                    mRoomViewModel.getAllPoligons(fieldTable.getNameField()).observe(getBaseActivity(), polygonTables -> {
                        assert polygonTables != null;
                        for (PolygonTable polygonTable : polygonTables) {
                            polygon.add(new LatLng(polygonTable.getMyLatitude(),polygonTable.getMyLongitude()));
                            Log.e("MapboxMapgetAll",polygonTable.getNameMyField()+ " "+polygonTable.getMyLatitude()+" ");

                        }
                        mapboxMap.addPolygon(new PolygonOptions()
                                .addAll(polygon)
                                .fillColor(Color.parseColor("#3bb2d0")));
                    });
                }

            });
            mRoomViewModel.getFieldsAndPolygons(mFragmentViewModel.getmDepartment().getValue()).observe(getBaseActivity(), new Observer<List<FieldsAndPolygons>>() {
                @Override
                public void onChanged(@Nullable List<FieldsAndPolygons> fieldsAndPolygons) {
                    assert fieldsAndPolygons != null;
                    for (FieldsAndPolygons polygon : fieldsAndPolygons) {
                        Log.e("FieldsAndPolygons",polygon.getNameMyField());
                    }

                }
            });

        }else {
            mRoomViewModel.getAllPoligons(mFragmentViewModel.getmField().getValue()).observeForever(polygonTables -> {
                assert polygonTables != null;
                for (PolygonTable polygonTable : polygonTables) {
                polygon.add(new LatLng(polygonTable.getMyLatitude(),polygonTable.getMyLongitude()));
                    Log.e("MapboxMapgetAllLatLng",polygonTable.getNameMyField()+ " "+polygonTable.getMyLatitude()+" ");

                }
                mapboxMap.addPolygon(new PolygonOptions()
                        .addAll(polygon)
                        .fillColor(Color.parseColor("#3bb2d0")));
        });

        }

    }

    @Override
    public void onPolygonClick(@NonNull Polygon polygon) {
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.INFO_POLYGON_FRAGMENT);
    }

    @Override
    public void backPressed() {
        navigatorManager.getMainManager(this).removeFragment();
    }
    //
//    @Override
//    public void onResume() {
//        super.onResume();
//        mapView.onResume();
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        mapView.onStart();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        mapView.onStop();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        mapView.onPause();
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        mapView.onSaveInstanceState(outState);
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mapView.onLowMemory();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mapView.onDestroy();
//    }

}
