package com.example.yurii.mydepartment.room.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.PrimaryKey;

public class FieldsAndPolygons {

    private String nameField;

    private String nameMyDepartment;

    @ColumnInfo(name = "nameMyField")
    private String nameMyField;

    @ColumnInfo(name = "myLongitude")
    private double myLongitude;

    @ColumnInfo(name = "myLatitude")
    private double myLatitude;

    public void setNameField(String nameField) {
        this.nameField = nameField;
    }

    public void setNameMyDepartment(String nameMyDepartment) {
        this.nameMyDepartment = nameMyDepartment;
    }

    public void setNameMyField(String nameMyField) {
        this.nameMyField = nameMyField;
    }

    public void setMyLongitude(double myLongitude) {
        this.myLongitude = myLongitude;
    }

    public void setMyLatitude(double myLatitude) {
        this.myLatitude = myLatitude;
    }

    public String getNameField() {
        return nameField;
    }

    public String getNameMyDepartment() {
        return nameMyDepartment;
    }

    public String getNameMyField() {
        return nameMyField;
    }

    public double getMyLongitude() {
        return myLongitude;
    }

    public double getMyLatitude() {
        return myLatitude;
    }
}
