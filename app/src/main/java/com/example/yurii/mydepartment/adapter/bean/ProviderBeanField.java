package com.example.yurii.mydepartment.adapter.bean;

import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.adapter.track.FieldAdapter;
import com.example.yurii.mydepartment.ui.activity.core.BaseActivity;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

@EBean
public class ProviderBeanField {

    @RootContext
    BaseActivity baseActivity;

    @Bean
    FieldAdapter fieldAdapter;


    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    @AfterViews
    public void initTrackAdapter(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(baseActivity));
        recyclerView.setAdapter(fieldAdapter);

    }

}
