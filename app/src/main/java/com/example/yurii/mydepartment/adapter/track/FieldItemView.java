package com.example.yurii.mydepartment.adapter.track;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.ui.dialog.ProgressDialogInMyApp;
import com.example.yurii.mydepartment.room.RoomViewModel;
import com.example.yurii.mydepartment.room.model.FieldTable;
import com.example.yurii.mydepartment.ui.navigator.NavigatorManager;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.item_field)
public class FieldItemView extends LinearLayout {
    @Bean
    protected NavigatorManager navigatorManager;

    Context myContext;
    ProgressDialogInMyApp progressDialogInMyApp;
    RoomViewModel mRoomViewModel;
    @ViewById(R.id.nameField)
    TextView trArtist;

    FieldTable fieldTable;


    @AfterViews
    public void init() {
    }

    @Click(R.id.nameField)
    public void OnClick() {
//        progressDialogInMyApp.showDialog();


    }
//    @LongClick(R.id.itemTrackInList)
//    public void OnLongClick(){
//        trackExoFinder.seekTo(null);
//    }

    public FieldItemView(Context context) {
        super(context);
        myContext = context;
        this.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
//        mRoomViewModel =  ViewModelProviders.of((FragmentActivity) context).get(RoomViewModel.class);

    }

    public void bind(FieldTable track) {
        fieldTable = track;
        trArtist.setText(track.getNameField());
//        progressDialogInMyApp = new ProgressDialogInMyApp((FragmentActivity) myContext,fieldTable.getNameField(),1);

    }


}
