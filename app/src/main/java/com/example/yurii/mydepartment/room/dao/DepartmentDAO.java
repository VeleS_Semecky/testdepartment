package com.example.yurii.mydepartment.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.yurii.mydepartment.room.model.DepartmentTable;
import com.example.yurii.mydepartment.room.model.DepartmentWithField;

import java.util.List;

@Dao
public interface DepartmentDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllDepartmen(List<DepartmentTable> people);

    @Delete
    void delete(DepartmentTable person);


    @Query("SELECT * FROM department_table")
    LiveData<List<DepartmentTable>> getAllDepartment();


}
