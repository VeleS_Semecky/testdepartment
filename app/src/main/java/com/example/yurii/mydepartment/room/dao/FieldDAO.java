package com.example.yurii.mydepartment.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.yurii.mydepartment.room.model.FieldTable;
import com.example.yurii.mydepartment.room.model.FieldWithPolygon;
import com.example.yurii.mydepartment.room.model.FieldsAndPolygons;

import java.util.List;
@Dao
public interface FieldDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllField(List<FieldTable> people);

    @Delete
    void delete(FieldTable person);


//    @Query("SELECT * FROM field_table WHERE idDepartment IS :idDepartment")
//    LiveData<List<FieldTable>> getAllField(int idDepartment);

    @Query("SELECT * FROM field_table ")
    LiveData<List<FieldTable>> getAllFields();

    @Query("SELECT * FROM field_table WHERE nameField IS :nameField")
    LiveData<List<FieldTable>> getField(String nameField);

    @Query("SELECT * FROM field_table WHERE nameMyDepartment IS :nameMyDepartment")
    LiveData<List<FieldTable>> getFieldNameDepartment(String nameMyDepartment);

    @Query("SELECT field_table.nameField ,field_table.nameMyDepartment,polygon_table.nameMyField,polygon_table.myLongitude,polygon_table.myLatitude" +
            "  FROM field_table, polygon_table" +
            " WHERE field_table.nameMyDepartment IS :nameMyDepartment")
    LiveData<List<FieldsAndPolygons>> getFieldsAndPolygons(String nameMyDepartment);
}