package com.example.yurii.mydepartment.room.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Relation;

import java.lang.reflect.Field;
import java.util.List;

public class DepartmentWithField {
    public  int id = 0;

    public   String nameDepartment;

    @Relation(parentColumn = "nameDepartment", entityColumn = "nameMyDepartment", entity = Field.class)
    public List<FieldWithPolygon> fieldWithPolygon;
}
