package com.example.yurii.mydepartment.ui.dialog;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.yurii.mydepartment.room.RoomViewModel;
import com.example.yurii.mydepartment.room.model.DepartmentTable;
import com.example.yurii.mydepartment.room.model.FieldTable;
import com.example.yurii.mydepartment.ui.fragment.core.FragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Юрий on 20.02.2018.
 */

public class ProgressDialogInMyApp {

    public void setDialogObservable(Flowable<TextView> dialogObservable) {
        this.dialogObservable = dialogObservable;
    }

    private Flowable<TextView> dialogObservable;

    private String name;


    public ArrayList<String> getAllDepartments() {
        return allDepartments;
    }


    ArrayList<String> studentPoligon = new ArrayList<>();
    ArrayList<String> allDepartments = new ArrayList<>();
    ArrayList<FieldTable> fieldTableArrayList = new ArrayList<>();

    public String getName() {
        return name;
    }
    CardView cardField;

    private AlertDialog.Builder adb;
    private AlertDialog b;

    private RoomViewModel mRoomViewModel;
    private FragmentViewModel mFragmentViewModel;

    private int checkedItemField =  0; // cow
    private int checkedItemDepartment = 0; // cow

    public ProgressDialogInMyApp(Activity activity,CardView card) {
        adb = new AlertDialog.Builder(activity);
        cardField = card;
        mRoomViewModel = ViewModelProviders.of((FragmentActivity) activity).get(RoomViewModel.class);
        mFragmentViewModel = ViewModelProviders.of((FragmentActivity) activity).get(FragmentViewModel.class);
    }

    public void showDialog() {
        b.show();
    }

    public void finishDialog() {
//        if(create)
        b.cancel();
        b.dismiss();
        cardField.setVisibility(View.VISIBLE);
    }

    public void getAllRXPoligons() {

        mRoomViewModel.getAllFields().observeForever(fieldTables -> {
            studentPoligon = new ArrayList<>();
            assert fieldTables != null;
            for (FieldTable polygonTable : fieldTables) {
                fieldTableArrayList.add(polygonTable);
                studentPoligon.add(polygonTable.getNameField());
            }
        });
        mRoomViewModel.getAllDepartments().observeForever((List<DepartmentTable> departmentTables) -> {
            assert departmentTables != null;
            allDepartments = new ArrayList<>();
            for (DepartmentTable departmentTable : departmentTables) {
                allDepartments.add(departmentTable.getNameDepartment());
            }
        });
    }

    public void create(ArrayList<String> arrayList,int i) {
        int checkedItem = i==0 ? checkedItemDepartment : checkedItemField;
        adb.setSingleChoiceItems(arrayList.toArray(new String[arrayList.size()]), checkedItem, (dialog, which) -> {
            checkedItemDepartment = i==0 ? which : checkedItemDepartment;
            checkedItemField = i==1 ? which : checkedItemField;
            Log.e("studentPoligon", arrayList.get(which));
            mFragmentViewModel.getmDepartment().postValue(i==0 ? arrayList.get(which) : name);
            mFragmentViewModel.getmField().postValue(i==1 ? arrayList.get(which) : "");
            dialogObservable.subscribe(s -> {
                s.setText(arrayList.get(which));

                name = i==0 ? arrayList.get(which) : name;
            }, throwable -> {
            }, () -> {
            });
            finishDialog();
        });
        b = adb.create();
    }

    public ArrayList<String> stringList(String s) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (FieldTable fieldTable : fieldTableArrayList) {
            if (fieldTable.getNameMyDepartment().equals(s)) {
                arrayList.add(fieldTable.getNameField());
            }
        }
        return arrayList;
    }
}
