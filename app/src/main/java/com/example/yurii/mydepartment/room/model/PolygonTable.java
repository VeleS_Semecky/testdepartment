package com.example.yurii.mydepartment.room.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.example.yurii.mydepartment.room.model.FieldTable;

@Entity(foreignKeys = @ForeignKey(
        entity = FieldTable.class,
        parentColumns = "nameField",
        childColumns = "nameMyField"),tableName = "polygon_table",indices = {@Index("nameMyField")})
public class PolygonTable {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private int id = 0;

    @ColumnInfo(name = "nameMyField")
    private String nameMyField;



    @ColumnInfo(name = "myLongitude")
    private double myLongitude;

    @ColumnInfo(name = "myLatitude")
    private double myLatitude;

    public PolygonTable(String nameMyField, double myLatitude, double myLongitude) {
        this.nameMyField = nameMyField;
        this.myLongitude = myLongitude;
        this.myLatitude = myLatitude;
    }

    public String getNameMyField() {
        return nameMyField;
    }

    public void setNameMyField(String nameMyField) {
        this.nameMyField = nameMyField;
    }

    public double getMyLongitude() {
        return myLongitude;
    }

    public void setMyLongitude(double myLongitude) {
        this.myLongitude = myLongitude;
    }

    public double getMyLatitude() {
        return myLatitude;
    }

    public void setMyLatitude(double myLatitude) {
        this.myLatitude = myLatitude;
    }
}
