package com.example.yurii.mydepartment.room.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Relation;

import java.util.List;

public class FieldWithPolygon {
    public  int id = 0;

    public  String nameField;

    public  String nameMyDepartment;

    @Relation(parentColumn = "nameField", entityColumn = "nameMyField")
    public List<PolygonTable> employees;
}
