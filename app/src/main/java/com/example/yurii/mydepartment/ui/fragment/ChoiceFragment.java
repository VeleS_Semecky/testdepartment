package com.example.yurii.mydepartment.ui.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.adapter.bean.finder.AddFieldInAdapter;
import com.example.yurii.mydepartment.ui.dialog.ProgressDialogInMyApp;
import com.example.yurii.mydepartment.room.RoomViewModel;
import com.example.yurii.mydepartment.ui.fragment.core.BaseFragment;
import com.example.yurii.mydepartment.ui.fragment.core.FragmentViewModel;
import com.example.yurii.mydepartment.ui.navigator.core.ResourceManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import io.reactivex.Flowable;

@EFragment(R.layout.fragment_choice)
public class ChoiceFragment extends BaseFragment{

    @Bean
    AddFieldInAdapter addFieldInAdapter;

    @ViewById(R.id.nameDepartment)
    TextView nameDepartment;
    @ViewById(R.id.nameField)
    TextView nameField;
    @ViewById(R.id.cardField)
    CardView cardField;
    ProgressDialogInMyApp progressDialogDepartment;
    Flowable<TextView> dialogObservable;
    FragmentViewModel mFragmentViewModel;





    @AfterViews
    public void initToolbar(){
        navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.SIMPLE, getBaseActivity().getResources().getText(R.string.app_name));

//        addElement();
        mFragmentViewModel = ViewModelProviders.of(getBaseActivity()).get(FragmentViewModel.class);
        progressDialogDepartment = new ProgressDialogInMyApp(getBaseActivity(),cardField);
        cardField.setVisibility(mFragmentViewModel.getmField().getValue()!=null ? View.VISIBLE : View.GONE);

        progressDialogDepartment.getAllRXPoligons();
        nameDepartment.setText( mFragmentViewModel.getmDepartment().getValue()!=null ? mFragmentViewModel.getmDepartment().getValue() :getBaseActivity().getResources().getString(R.string.choice_department));
        nameField.setText( mFragmentViewModel.getmField().getValue()!=null ? mFragmentViewModel.getmField().getValue() :getBaseActivity().getResources().getString(R.string.choice_field));
    }


    @Click(R.id.cardDepartment)
    public void onClickTxDepartment(){
        dialogObservable = Flowable.just(nameDepartment);
        progressDialogDepartment.setDialogObservable(dialogObservable);
        progressDialogDepartment.create(progressDialogDepartment.getAllDepartments(),0);
        progressDialogDepartment.showDialog();
//        cardField.setVisibility(View.VISIBLE);
    }
    @Click(R.id.cardField)
    public void onClickTxField(){
        dialogObservable = Flowable.just(nameField);
        progressDialogDepartment.setDialogObservable(dialogObservable);
        progressDialogDepartment.create(progressDialogDepartment.stringList(progressDialogDepartment.getName()),1);
        progressDialogDepartment.showDialog();
        Log.e("Department",nameDepartment.getText().toString());
//        mFragmentViewModel.getmDepartment().postValue(nameDepartment.getText().toString());
//        mFragmentViewModel.getmField().postValue(nameField.getText().toString());
    }
    @Click(R.id.fabMapBox)
    protected void onClickFabMapBox(){
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.MAPBOX_FRAGMENT);
//        mFragmentViewModel.getmDepartment().postValue(nameDepartment.getText().toString());
//        mFragmentViewModel.getmField().postValue(nameField.getText().toString());
    }



    public void addElement(){
        RoomViewModel mRoomViewModel;
        // Get a new or existing ViewModel from the ViewModelProvider.
        mRoomViewModel = ViewModelProviders.of(getBaseActivity()).get(RoomViewModel.class);
        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mRoomViewModel.insertDepartmentTable(addFieldInAdapter.setDepartmentTables());
        mRoomViewModel.insertFieldTable(addFieldInAdapter.setFieldTables());
        mRoomViewModel.insertPolygonTable(addFieldInAdapter.setPolygonTables());
    }

}