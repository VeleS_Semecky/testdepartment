package com.example.yurii.mydepartment.ui.navigator.core;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ResourceManager {
    public static final class ActivityId{
        //1-20
        public final static int START_ACTIVITY = 1;


    }

    public static final class FragmentId{
        //100-500
        public final static int LIST_FIELD_FRAGMENT = 100;
        public final static int CHOICE_FRAGMENT = 101;
        public final static int MAPBOX_FRAGMENT = 102;
        public final static int INFO_POLYGON_FRAGMENT = 103;

    }

    public static final class ToolbarId{
        //1000 - 1500
        public final static int SIMPLE = 1000;
        public final static int SEARCH = 1001;


    }
}
