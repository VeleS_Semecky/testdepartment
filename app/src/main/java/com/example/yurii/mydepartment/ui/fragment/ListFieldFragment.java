package com.example.yurii.mydepartment.ui.fragment;


import android.app.SearchManager;
import android.content.Context;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.adapter.bean.ProviderBeanField;
import com.example.yurii.mydepartment.ui.fragment.core.BaseFragment;
import com.example.yurii.mydepartment.ui.navigator.core.ResourceManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InjectMenu;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_track_start)
//@OptionsMenu(R.menu.toolbar_activity_dictionary)
public class ListFieldFragment extends BaseFragment implements SearchView.OnQueryTextListener {


//    @ViewById(R.id.searching)
//    SearchView searchView;


    @Bean
    ProviderBeanField providerBeanField;
    @AfterViews
    public void initToolbar(){
//        Toolbar toolbar = (Toolbar) getBaseActivity().findViewById(R.id.toolbar);
//        getBaseActivity().setSupportActionBar(toolbar);
        navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.SIMPLE, R.string.app_name);
//        SearchManager searchManager = (SearchManager) getBaseActivity().getSystemService(Context.SEARCH_SERVICE);
//        searchView.setMaxWidth(Integer.MAX_VALUE);
////        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Toast.makeText(getBaseActivity(),"Our word : "+query,Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Toast.makeText(getBaseActivity(),"Our words : "+newText,Toast.LENGTH_SHORT).show();
        return false;
    }
}
