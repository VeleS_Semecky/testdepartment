package com.example.yurii.mydepartment.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.ui.activity.core.BaseActivity;
import com.example.yurii.mydepartment.ui.fragment.MapBoxFragment_;
import com.example.yurii.mydepartment.ui.navigator.NavigatorManager;
import com.example.yurii.mydepartment.ui.navigator.core.ResourceManager;
import com.example.yurii.mydepartment.ui.navigator.core.ResourceNames;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.WindowFeature;

@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    @Bean
    public NavigatorManager navigatorManager;

    @InstanceState
    Integer save = 0;

    @AfterViews
    public void initView(){
//        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.LIST_FIELD_FRAGMENT);
        if(save == 0) {
            navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.CHOICE_FRAGMENT);
            save = 1;
        }
//        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.INFO_POLYGON_FRAGMENT);
//        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.MAPBOX_FRAGMENT);
//        getFragmentManager().beginTransaction().replace(R.id.containerToolbar, MapBoxFragment_.builder().build(), ResourceNames.MAPBOX_FRAGMENT).commit();

    }
}
