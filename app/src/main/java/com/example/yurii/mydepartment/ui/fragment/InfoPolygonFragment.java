package com.example.yurii.mydepartment.ui.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.widget.TextView;

import com.example.yurii.mydepartment.R;
import com.example.yurii.mydepartment.room.RoomViewModel;
import com.example.yurii.mydepartment.room.model.PolygonTable;
import com.example.yurii.mydepartment.ui.fragment.core.BaseFragment;
import com.example.yurii.mydepartment.ui.fragment.core.FragmentViewModel;
import com.example.yurii.mydepartment.ui.navigator.core.ResourceManager;
import com.mapbox.mapboxsdk.geometry.LatLng;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EFragment(R.layout.fragment_info_polygon)
public class InfoPolygonFragment extends BaseFragment{

    @ViewById( R.id.txt_Field )
    TextView txt_Field;
    @ViewById( R.id.textLatitude )
    TextView textLatitude;
    @ViewById( R.id.textLongitude )
    TextView textLongitude;
    @InstanceState
    String thLatitude;
    @InstanceState
    String thLongitude;
    protected FragmentViewModel mFragmentViewModel;
    protected RoomViewModel mRoomViewModel;
    @AfterViews
    public void init(){
        mFragmentViewModel = ViewModelProviders.of(getBaseActivity()).get(FragmentViewModel.class);
        mRoomViewModel = ViewModelProviders.of(getBaseActivity()).get(RoomViewModel.class);
        navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.SIMPLE, mFragmentViewModel.getmDepartment().getValue());
        String thField = mFragmentViewModel.getmField().getValue();
        txt_Field.setText(thField);
        List<LatLng> polygon = new ArrayList<>();
        mRoomViewModel.getAllPoligons(thField).observeForever(polygonTables -> {
            assert polygonTables != null;
            thLatitude = "Latitude"+ "\n";
            thLongitude = "Longitude"+ "\n";
            for (PolygonTable polygonTable : polygonTables) {
                polygon.add(new LatLng(polygonTable.getMyLatitude(),polygonTable.getMyLongitude()));
                thLatitude = thLatitude + polygonTable.getMyLatitude() + "\n";
                thLongitude = thLongitude + polygonTable.getMyLongitude() + "\n";
            }
            textLatitude.setText(thLatitude);
            textLongitude.setText(thLongitude);
        });

    }

}
